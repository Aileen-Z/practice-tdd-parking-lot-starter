## 1、Objective
Things we have learned in week 1: Concept map, Tasking, TDD, Java
(1)I learned how to coding with TDD(Test driven development) today. tasking -> list cases -> commit case code -> commit code.\
(1.1)There're 4 main steps to write a test case. Test Description(use should_return_xxx_when_xxx(fuction name)_given_xxx as test method name), Given(Prepare), When(execute), Then(verify).\
(1.2)Why TDD: Simple design, test as documentation, quick feedback, safety net.\

(2)Code Review ：A method should not exceed 15 lines. 
	
(3)Java : throwing exceptions, stream API, hashmap,oop

## 2、Reflective
useful

## 3、Interpretive
In the process of listing cases, I gained a new sense of how to disassemble business requirements from a programmer's perspective.

## 4、Decisional
In the code review, it is not very easy to understand the coding ideas that are different from my own. I will continue using Tasking and TDD for coding.
