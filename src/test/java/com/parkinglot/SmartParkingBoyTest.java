package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SmartParkingBoyTest {
    @Test
    void should_return_a_parking_tickets_and_park_in_the_lot_that_has_more_empty_positions_when_park_given_three_parking_lots_and_a_car(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 2);
        ParkingLot parkingLot2 = new ParkingLot(5, 1);
        ParkingLot parkingLot3 = new ParkingLot(5, 4);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2,parkingLot3);
        Car car = new Car();
        //when
        ParkingLot parkingLot = smartParkingBoy.chooseParkingLot(car);
        ParkingTicket parkingTicket = smartParkingBoy.park(car);
        //then
        Assertions.assertNotNull(parkingTicket);
        Assertions.assertEquals(parkingLot3, parkingLot);

    }

    @Test
    void should_return_a_parking_tickets_when_given_two_parking_lots_and_a_car(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 2);
        ParkingLot parkingLot2 = new ParkingLot(5, 2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingLot parkingLot = smartParkingBoy.chooseParkingLot(car);
        ParkingTicket parkingTicket = smartParkingBoy.park(car);
        //then
        Assertions.assertNotNull(parkingTicket);
        Assertions.assertEquals(parkingLot2, parkingLot);
    }

    @Test
    void should_return_no_positions_left_when_park_given_two_parking_lots_with_no_empty_positions_and_a_car(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 0);
        ParkingLot parkingLot2 = new ParkingLot(5, 0);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->smartParkingBoy.park(car));
    }

    @Test
    void should_return_two_right_cars_when_fetch_given_two_parking_lots_and_two_right_tickets(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 2);
        ParkingLot parkingLot2 = new ParkingLot(5, 3);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = smartParkingBoy.park(car1);
        ParkingTicket parkingTicket2 = smartParkingBoy.park(car2);
        //when
        Car car1Fetch = smartParkingBoy.fetch(parkingTicket1);
        Car car2Fetch = smartParkingBoy.fetch(parkingTicket2);

        //then
        Assertions.assertEquals(car1, car1Fetch);
        Assertions.assertEquals(car2, car2Fetch);
    }

    @Test
    void should_return_unrecognized_ticket_when_fetch_given_a_wrong_ticket_and_two_parking_lots(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 3);
        ParkingLot parkingLot2 = new ParkingLot(5, 2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        ParkingTicket parkingTicket = smartParkingBoy.park(car);
        ParkingTicket wrongParkingTicket = new ParkingTicket();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->smartParkingBoy.fetch(wrongParkingTicket));
    }

    @Test
    void should_return_unrecognized_ticket_when_fetch_given_a_used_ticket_and_two_parking_lots(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 3);
        ParkingLot parkingLot2 = new ParkingLot(5, 2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        ParkingTicket parkingTicket = smartParkingBoy.park(car);
        Car carFetched = smartParkingBoy.fetch(parkingTicket);
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->smartParkingBoy.fetch(parkingTicket));
    }
}
