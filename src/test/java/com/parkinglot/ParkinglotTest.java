package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkinglotTest {
    @Test
    void should_return_a_parking_ticket_when_park_given_a_car() throws Exception {
        ParkingLot parkingLot = new ParkingLot(10,10);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingLot.park(car);
        //then
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_a_right_car_when_fetch_given_a_parking_ticket() throws Exception {
        ParkingLot parkingLot = new ParkingLot(10,10);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        //when
        Car car1 = parkingLot.fetch(parkingTicket);
        //then
        Assertions.assertEquals(car, car1);
    }

    @Test
    void should_return_two_different_right_cars_when_fetch_given_two_diff_tickets() throws Exception {
        ParkingLot parkingLot = new ParkingLot(10, 10);
        Car car1 = new Car();
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        Car car2 = new Car();
        ParkingTicket parkingTicket2 = parkingLot.park(car2);
        //when
        Car car1Fetch = parkingLot.fetch(parkingTicket1);
        Car car2Fetch = parkingLot.fetch(parkingTicket2);
        //then
        Assertions.assertNotEquals(car1Fetch, car2Fetch);
        Assertions.assertEquals(car1, car1Fetch);
        Assertions.assertEquals(car2, car2Fetch);

    }

    @Test
    void should_return_err_when_fetch_given_a_wrong_parking_ticket() throws Exception {
        ParkingLot parkingLot = new ParkingLot(10, 10);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        ParkingTicket parkingTicket1 = new ParkingTicket();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->parkingLot.fetch(parkingTicket1));
    }

    @Test
    void should_return_err_when_fetch_given_a_used_parking_ticket(){
        ParkingLot parkingLot = new ParkingLot(10, 10);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        Car car1 = parkingLot.fetch(parkingTicket);
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->parkingLot.fetch(parkingTicket));

    }

    @Test
    void should_return_err_when_park_given_no_positions_left_in_the_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(10, 0);
        Car car = new Car();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->parkingLot.park(car));
    }


}
