package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SuperParkingBoyTest {
    @Test
    void should_return_a_ticket_and_park_in_the_lot_with_highest_position_rate_when_park_given_three_parking_lots_and_a_car(){
        ParkingLot parkingLot1 = new ParkingLot(5, 3);
        ParkingLot parkingLot2 = new ParkingLot(5, 4);
        ParkingLot parkingLot3 = new ParkingLot(5, 1);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2, parkingLot3);
        Car car = new Car();

        //when
        ParkingLot parkingLot = superParkingBoy.chooseParkingLot(car);
        ParkingTicket parkingTicket = superParkingBoy.park(car);
        //then
        Assertions.assertNotNull(parkingTicket);
        Assertions.assertEquals(parkingLot2, parkingLot);
    }
    @Test
    void should_return_a_parking_tickets_when_given_two_parking_lots_and_a_car(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 2);
        ParkingLot parkingLot2 = new ParkingLot(5, 2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingLot parkingLot = superParkingBoy.chooseParkingLot(car);
        ParkingTicket parkingTicket = superParkingBoy.park(car);
        //then
        Assertions.assertNotNull(parkingTicket);
        Assertions.assertEquals(parkingLot2, parkingLot);
    }

    @Test
    void should_return_no_positions_left_when_park_given_two_parking_lots_with_no_empty_positions_and_a_car(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 0);
        ParkingLot parkingLot2 = new ParkingLot(5, 0);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->superParkingBoy.park(car));
    }

    @Test
    void should_return_two_right_cars_when_fetch_given_two_parking_lots_and_two_right_tickets(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 2);
        ParkingLot parkingLot2 = new ParkingLot(5, 3);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = superParkingBoy.park(car1);
        ParkingTicket parkingTicket2 = superParkingBoy.park(car2);
        //when
        Car car1Fetch = superParkingBoy.fetch(parkingTicket1);
        Car car2Fetch = superParkingBoy.fetch(parkingTicket2);

        //then
        Assertions.assertEquals(car1, car1Fetch);
        Assertions.assertEquals(car2, car2Fetch);
    }

    @Test
    void should_return_unrecognized_ticket_when_fetch_given_a_wrong_ticket_and_two_parking_lots(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 3);
        ParkingLot parkingLot2 = new ParkingLot(5, 2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        ParkingTicket parkingTicket = superParkingBoy.park(car);
        ParkingTicket wrongParkingTicket = new ParkingTicket();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->superParkingBoy.fetch(wrongParkingTicket));
    }

    @Test
    void should_return_unrecognized_ticket_when_fetch_given_a_used_ticket_and_two_parking_lots(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 3);
        ParkingLot parkingLot2 = new ParkingLot(5, 2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        ParkingTicket parkingTicket = superParkingBoy.park(car);
        Car carFetched = superParkingBoy.fetch(parkingTicket);
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->superParkingBoy.fetch(parkingTicket));
    }
}
