package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StandardParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_given_a_car_and_a_parking_lot(){
        ParkingLot parkingLot = new ParkingLot(10, 10);

        Car car = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        //when
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        //then
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_a_right_car_when_fetch_given_a_parking_ticket_and_a_parking_lot(){
        ParkingLot parkingLot = new ParkingLot(10, 10);
        Car car = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        //when
        Car carFetched = standardParkingBoy.fetch(parkingTicket);
        //then
        Assertions.assertEquals(car, carFetched);
    }

    @Test
    void should_return_two_right_cars_when_fetch_given_two_parking_tickets_and_a_parking_lot(){
        ParkingLot parkingLot = new ParkingLot(10, 10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        Car car1 = new Car();
        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);
        Car car2 = new Car();
        ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);
        //when
        Car car1Fetched = standardParkingBoy.fetch(parkingTicket1);
        Car car2Fetched = standardParkingBoy.fetch(parkingTicket2);
        //then
        Assertions.assertNotEquals(car1Fetched, car2Fetched);
    }

    @Test
    void should_return_err_when_fetch_given_a_wrong_parking_ticket_and_a_parking_lot() throws Exception {
        ParkingLot parkingLot = new ParkingLot(10, 10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        Car car = new Car();
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        ParkingTicket parkingTicket1 = new ParkingTicket();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->standardParkingBoy.fetch(parkingTicket1));
    }

    @Test
    void should_return_err_when_fetch_given_a_used_parking_ticket_and_a_parking_lot(){
        ParkingLot parkingLot = new ParkingLot(10, 10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        Car car = new Car();
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        Car car1 = standardParkingBoy.fetch(parkingTicket);
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->standardParkingBoy.fetch(parkingTicket));

    }

    @Test
    void should_return_err_when_park_given_no_positions_left_in_the_parking_lot_and_a_car() {
        ParkingLot parkingLot = new ParkingLot(10, 0);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        Car car = new Car();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->standardParkingBoy.park(car));
    }

    @Test
    void should_return_a_ticket_and_park_in_the_first_lot_when_park_given_two_parking_lots_with_a_car_each(){
        ParkingLot parkingLot1 = new ParkingLot(5, 3);
        ParkingLot parkingLot2 = new ParkingLot(5, 3);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingLot parkingLot = standardParkingBoy.chooseParkingLot(car);
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        //then
        Assertions.assertEquals(parkingLot1, parkingLot);
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_a_ticket_and_park_in_the_second_lot_when_park_given_two_parking_lots_and_the_first_one_is_full(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 0);
        ParkingLot parkingLot2 = new ParkingLot(5, 3);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingLot parkingLot = standardParkingBoy.chooseParkingLot(car);
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        //then
        Assertions.assertEquals(parkingLot2, parkingLot);
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_two_right_cars_when_fetch_given_two_diff_tickets_and_two_parking_lots(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 3);
        ParkingLot parkingLot2 = new ParkingLot(5, 3);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLotFirstChoose = standardParkingBoy.chooseParkingLot(car1);
        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);
        ParkingLot parkingLotSecondChoose = standardParkingBoy.chooseParkingLot(car2);
        ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);
        //when
        Car car1Fetch = standardParkingBoy.fetch(parkingTicket1);
        Car car2Fetch = standardParkingBoy.fetch(parkingTicket2);
        //then
        Assertions.assertEquals(car1, car1Fetch);
        Assertions.assertEquals(car2, car2Fetch);
    }

    @Test
    void should_return_unrecognized_ticket_when_fetch_given_a_wrong_ticket_and_two_parking_lots(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 3);
        ParkingLot parkingLot2 = new ParkingLot(5, 3);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        ParkingLot parkingLot = standardParkingBoy.chooseParkingLot(car);
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        ParkingTicket parkingTicket1 = new ParkingTicket();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->standardParkingBoy.fetch(parkingTicket1));
    }

    @Test
    void should_return_unrecognized_ticket_when_fetch_given_a_used_ticket_and_two_parking_lots(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5 ,3);
        ParkingLot parkingLot2 = new ParkingLot(5, 3);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        ParkingLot parkingLot = standardParkingBoy.chooseParkingLot(car);
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        Car car1 = standardParkingBoy.fetch(parkingTicket);
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->standardParkingBoy.fetch(parkingTicket));
    }


    @Test
    void should_return_no_positions_left_when_park_given_two_full_parking_lots_and_a_car(){
        //given
        ParkingLot parkingLot1 = new ParkingLot(5, 0);
        ParkingLot parkingLot2 = new ParkingLot(5, 0);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when

        //then
        Assertions.assertThrows(RuntimeException.class, ()->standardParkingBoy.park(car));
    }
}
