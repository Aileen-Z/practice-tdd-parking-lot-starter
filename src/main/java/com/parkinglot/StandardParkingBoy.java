package com.parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StandardParkingBoy {
    List<ParkingLot> parkingLotList;
//    ParkingLot parkingLot = new ParkingLot(10);

//    StandardParkingBoy(){}

    StandardParkingBoy(ParkingLot... parkingLots){
        this.parkingLotList = Arrays.asList(parkingLots);
    }

    public ParkingTicket park(Car car) {
        ParkingTicket parkingTicket = this.chooseParkingLot(car).park(car);
        return parkingTicket;
    }

    public ParkingLot chooseParkingLot(Car car) {
        for(ParkingLot parkingLot: parkingLotList){
            if(parkingLot.emptyPositions != 0){
                return parkingLot;
            }
        }
        throw new RuntimeException("No available positions.");
    }

    public Car fetch(ParkingTicket parkingTicket) {
        for(ParkingLot parkingLot : parkingLotList){
            if(parkingLot.ticketCarHashMap.containsKey(parkingTicket)){
                Car car = parkingLot.ticketCarHashMap.get(parkingTicket);
                parkingLot.ticketCarHashMap.remove(parkingTicket);
                parkingLot.emptyPositions += 1;
                return car;
            }
        }
        throw new RuntimeException("Unrecognized parking ticket.");
    }
}
