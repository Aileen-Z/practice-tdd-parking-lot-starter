package com.parkinglot;

import java.util.*;
import java.util.stream.Collectors;

public class SuperParkingBoy {
    List<ParkingLot> parkingLotList;
    SuperParkingBoy(ParkingLot... parkingLots){this.parkingLotList = Arrays.asList(parkingLots);}

    public ParkingTicket park(Car car) {
        ParkingTicket parkingTicket = this.chooseParkingLot(car).park(car);
        return parkingTicket;
    }

    public ParkingLot chooseParkingLot(Car car) {
        ParkingLot parkingLot = sortEmptyPositionsRates();
        if(parkingLot.emptyPositions != 0){
            return parkingLot;
        }
        throw new RuntimeException("No available positions.");
    }

    public ParkingLot sortEmptyPositionsRates(){
        Map<Double, ParkingLot> emptyPositionAndParkingLotHashMap = new HashMap<Double, ParkingLot>();
        List<Double> rates = new ArrayList<Double>();
        for(ParkingLot parkingLot: parkingLotList){
            emptyPositionAndParkingLotHashMap.put(parkingLot.getPositionRate(), parkingLot);
            rates.add(parkingLot.getPositionRate());
        }
        double maxRate = Collections.max(rates);
        return emptyPositionAndParkingLotHashMap.get(maxRate);
    }

    public Car fetch(ParkingTicket parkingTicket) {
        for(ParkingLot parkingLot : parkingLotList){
            if(parkingLot.ticketCarHashMap.containsKey(parkingTicket)){
                Car car = parkingLot.ticketCarHashMap.get(parkingTicket);
                parkingLot.ticketCarHashMap.remove(parkingTicket);
                parkingLot.emptyPositions += 1;
                return car;
            }
        }
        throw new RuntimeException("Unrecognized parking ticket.");
    }
}
