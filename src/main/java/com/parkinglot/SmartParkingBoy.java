package com.parkinglot;

import java.util.*;
import java.util.stream.Collectors;

public class SmartParkingBoy {
    List<ParkingLot> parkingLotList;
    SmartParkingBoy(ParkingLot... parkingLots){this.parkingLotList = Arrays.asList(parkingLots);}

    public ParkingTicket park(Car car) {
        ParkingTicket parkingTicket = this.chooseParkingLot(car).park(car);
        return parkingTicket;
    }

    public ParkingLot chooseParkingLot(Car car) {
        ParkingLot parkingLot = sortEmptyPositions();
        if(parkingLot.emptyPositions != 0){
            return parkingLot;
        }
        throw new RuntimeException("No available positions.");
    }

    public ParkingLot sortEmptyPositions(){
        Map<Integer, ParkingLot> emptyPositionAndParkingLotHashMap = new HashMap<Integer, ParkingLot>();
        for(ParkingLot parkingLot: parkingLotList){
            emptyPositionAndParkingLotHashMap.put(parkingLot.emptyPositions, parkingLot);
        }
        Map<Integer, ParkingLot> sortedMap = emptyPositionAndParkingLotHashMap.entrySet().stream()
                .sorted(Map.Entry.<Integer, ParkingLot>comparingByKey().reversed())
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue,
                                (oldVal, newVal) -> oldVal,
                                LinkedHashMap::new
                        )
                );
        ParkingLot parkingLot = sortedMap.entrySet().stream().findFirst().get().getValue();
        return parkingLot;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        for(ParkingLot parkingLot : parkingLotList){
            if(parkingLot.ticketCarHashMap.containsKey(parkingTicket)){
                Car car = parkingLot.ticketCarHashMap.get(parkingTicket);
                parkingLot.ticketCarHashMap.remove(parkingTicket);
                parkingLot.emptyPositions += 1;
                return car;
            }
        }
        throw new RuntimeException("Unrecognized parking ticket.");
    }
}
