package com.parkinglot;

import java.text.DecimalFormat;
import java.util.HashMap;

public class ParkingLot {
    public int capacity;
    public int emptyPositions;
    public double positionRate;
    HashMap<ParkingTicket, Car> ticketCarHashMap = new HashMap<>();

    ParkingLot(int capacity,int emptyPositions){
        this.capacity = capacity;
        this.emptyPositions = emptyPositions;
    }

    public double getPositionRate() {
//        DecimalFormat df = new DecimalFormat("#.00");
//        df.format(emptyPositions / capacity);
        positionRate = (float)emptyPositions / capacity;
        return positionRate;
    }

    public ParkingTicket park(Car car) {
        if(emptyPositions == 0){
            throw new RuntimeException("No available position.");
        }else{
            ParkingTicket parkingTicket = new ParkingTicket();
            ticketCarHashMap.put(parkingTicket, car);
            this.emptyPositions -= 1;
            return parkingTicket;
        }
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if(ticketCarHashMap.containsKey(parkingTicket)){
            Car car = ticketCarHashMap.get(parkingTicket);
            ticketCarHashMap.remove(parkingTicket);
            this.emptyPositions += 1;
            return car;
        }else{
            throw new RuntimeException("Unrecognized parking ticket.");
        }


    }
}
